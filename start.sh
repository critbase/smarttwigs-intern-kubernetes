#!/usr/bin/env bash

# start minikube with 3 nodes
minikube start --nodes 3

# build the container image for the application
pushd docker
minikube image build -t python-app .
popd

# apply the configuration for the pod
kubectl apply -f ./python-app.yaml
